#!/usr/bin/env python3
"""Run a Turma script on some data."""
from __future__ import annotations

import argparse
from collections import deque
from itertools import islice
from typing import Literal, NamedTuple, cast


class ParseError(Exception):
    """Error during parsing a machine."""


class Tape:
    """An infinitely long list in both directions, which points at a
    particular value."""

    def __init__(self, data: str = ""):
        self.position = 0
        self.symbols = deque(reversed(data) if data else (None,))

    def read(self):
        """Get the current symbol under the head."""
        return self.symbols[self.position]

    def write(self, value: str):
        """Write a symbol under the head."""
        self.symbols[self.position] = value

    def move(self, direction: Literal["<", ">"]):
        """Move the head in a direction (< or >)."""
        if direction == ">":
            if self.position >= len(self.symbols) - 1:
                self.symbols.append(None)
            if self.position == 0 and self.symbols[0] is None:
                self.symbols.popleft()
            else:
                self.position += 1
        elif direction == "<":
            if self.position <= 0:
                self.symbols.appendleft(None)
                self.position += 1
            if self.position == len(self.symbols) - 1 and self.symbols[-1] is None:
                self.symbols.pop()
            self.position -= 1
        else:
            raise ValueError(f"Invalid direction: {direction}")

    def decode(self) -> str:
        """Get the output to the right of and under the head."""
        return "".join(
            i for i in islice(self.symbols, self.position, None) if i is not None
        )

    def __str__(self):
        strings = ["_" if x is None else x for x in self.symbols]
        head_pos = sum(len(x) for x in strings[: self.position])
        return "".join(strings) + "\n" + " " * head_pos + "^"

    def __repr__(self):
        return f"<Tape\n{self}\n>"


class Instruction(NamedTuple):
    write: str | None
    move: str
    new_state: str | None


def get_unicode_character(instructions: list[str]) -> str:
    """Pop eight hexadecimal characters, returning the unicode character
    they correspond to."""
    if len(instructions) < 8:
        raise ParseError("unicode escape sequence requires eight characters")
    try:
        unicode_value = instructions[:8]
        instructions = instructions[8:]
        return chr(int("".join(unicode_value), 16))
    except ValueError:
        raise ParseError(
            "invalid hexadecimal Unicode character: {unicode_value}"
        ) from None


class Token(NamedTuple):
    token: str


def tokenise(instructions) -> list[Token | str | None]:
    """Convert the instructions text into a list of tokens."""
    instructions = list(instructions)
    literal_underscore = False
    tokens: list[Token | str | None] = []
    token = ""
    while instructions:
        character = instructions.pop(0)
        # Escape sequences
        if character == "\\":
            literal_underscore = True
            literal_char = instructions.pop(0)
            if literal_char == "U":
                token += get_unicode_character(instructions)
            elif literal_char == "n":
                token += "\n"
            elif literal_char == "t":
                token += "\t"
            else:
                token += literal_char
        # Reserved tokens
        elif character in "<>{}":
            if token != "":
                if token == "_" and not literal_underscore:
                    tokens.append(None)
                else:
                    tokens.append(token)
            tokens.append(Token(character))
            token = ""
            literal_underscore = False
        # Any remaining non-whitespace is literal
        elif character not in "\n\t ":
            token += character
    if token:
        tokens.append(token)
    return tokens


def expect(tokens, expected, context):
    """Consume a token, raising an error if it is incorrect."""
    try:
        got = tokens.pop(0)
    except IndexError:
        got = "EOF"
    if got != expected:
        raise ParseError(f"expected {expected} {context} (got {got})")


def not_expect(tokens, expected, not_expected, context):
    """Check if the next token is in the banned list."""
    try:
        got = tokens[0]
    except IndexError:
        got = "EOF"

    if got in not_expected:
        raise ParseError(f"expected {expected} {context} (got {got})")


SPECIALS = [Token(x) for x in "<>{}"]


def parse_instruction(
    tokens: list[Token | str | None], instructions: dict[str | None, Instruction]
) -> None:
    """Parse an instruction, adding it to instruction."""
    not_expect(tokens, "instruction condition", SPECIALS, "after open brace")
    instruction_condition = cast(str | None, tokens.pop(0))
    expect(tokens, Token("{"), "after instruction condition")
    not_expect(tokens, "write value", SPECIALS, "after open brace")
    write_value = cast(str | None, tokens.pop(0))

    direction = tokens.pop(0)
    if direction not in {Token("<"), Token(">")}:
        raise ParseError(f"invalid direction: {direction}")
    direction = cast(Token, direction)
    not_expect(tokens, "new state", SPECIALS, "after direction")
    instructions[instruction_condition] = Instruction(
        write_value, direction.token, cast(str | None, tokens.pop(0))
    )
    expect(tokens, Token("}"), "after new state")


def parse_instructions(tokens) -> dict[str | None, Instruction]:
    """Parse all instructions for a state."""
    instructions: dict[str | None, Instruction] = {}
    while True:
        if tokens[0] == Token("}"):
            tokens.pop(0)
            break
        parse_instruction(tokens, instructions)
    return instructions


def parse(code: str) -> dict:
    """Parse an entire Turing Machine state table."""
    tokens = tokenise(code)
    states = {}
    while tokens:
        not_expect(tokens, "state name", SPECIALS, "for new state")
        state_name = tokens.pop(0)
        expect(tokens, Token("{"), "after state name")
        instructions = parse_instructions(tokens)
        # Cannot redefine null state
        if state_name is not None:
            states[state_name] = instructions
    return states


def run(states: dict, tape, verbosity=0) -> Tape:
    """Run a Turing machine on a tape."""
    state = next(iter(states))
    while True:
        if verbosity:
            print(str(tape))
            if verbosity > 1:
                print(f"state: {state}")
                print()
        try:
            instruction = states[state][tape.read()]
        except KeyError:
            return tape
        tape.write(instruction.write)
        if instruction.new_state not in states:
            return tape
        tape.move(instruction.move)
        state = instruction.new_state


def parse_and_run(code: str, input_data: str, verbosity=0) -> str:
    """Run a Turma script on some input data."""
    machine = parse(code)
    tape = run(states=machine, tape=Tape(input_data), verbosity=verbosity)
    return tape.decode()


def main():
    """Parse arguments, parse a Turma script, and run it."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "machine", type=argparse.FileType("r"), help="machine code to run"
    )
    parser.add_argument("-p", "--progress", action="store_true", help="show progress")
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="show the tape at every step (-vv for state also)",
    )
    parser.add_argument("input", type=str, default="", help="input data")
    args = parser.parse_args()
    try:
        print(parse_and_run(args.machine.read(), args.input, verbosity=args.verbose))
    except ParseError as error:
        parser.error(error)


if __name__ == "__main__":
    main()
