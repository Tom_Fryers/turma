# Turma

Turma is a programming language for writing Turing Machines.

## Properties

The input to the program is a Unicode string. This is read onto the
right of the tape one symbol per character, with the head over the first
character. The rest of the tape consists of infinitely many null
characters, which are represented with `_` (for a literal underscore use
`\_`). The Turing Machine halts when

- it enters the null state (`_`), or
- it tries to enter a state which has not been defined, or
- it reads a symbol for which no condition is present, or
- it executes an instruction which gives no new state.

It then concatenates everything to the right of or under the head and
returns it. Any string can be used as a symbol or state name.

## Syntax

A program consists of a list of states. The first state is the initial
state. The format for a state is as follows: `state name{instructions
list}`.

The each instruction is formatted as follows: `condition{write value>new
state}`. The greater than sign means move right. A less than sign would
mean move left.

### Escape Sequences

A reserved character (`{`, `}`, `<`, `>`, and `\`) can be escaped with a
backslash. Whitespace is usually ignored, so use `\ ` for a space, `\n`
for a newline and `\t` for a tab (literal newlines and tabs are
ignored). Use `\Uxxxxxxxx` for other characters. The eight xs should be
hexadecimal digits, giving the Unicode codepoint.

## Comments

You can effectively add a comment with `comment text{}` outside a state
definition, and `comment text{_>_}` inside a state definition.  These
comments are just code that probably does nothing, so they may have an
effect if you have states or symbols with the same names as the comment
text.

If you want a comment that is guaranteed to be ignored, use `_{comment
text{_>_}}`. This attempts to redefines the null state, which does
nothing.

## Example Programs

### Palindrome Checker

    read{
     0{_>got_0}
     1{_>got_1}
     _{0<out}
    }
    got_0{
     0{0>got_0}
     1{1>got_0}
     _{_<check_0}
    }
    got_1{
     0{0>got_1}
     1{1>got_1}
     _{_<check_1}
    }
     check_0{
     0{_<go_back}
     1{1<out}
     _{0<out}
    }
     check_1{
     1{_<go_back}
     0{1<out}
     _{0<out}
    }
    go_back{
     0{0<go_back}
     1{1<go_back}
     _{_>read}
    }
    out{
     _{_>_}
     0{_>_}
     1{_>_}
    }
